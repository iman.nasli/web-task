<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'content', 'image_path', 'status'
    ];

    public function tag()
    {
        return $this->belongsTo('App\Model\Tag', 'tag_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }
}
