<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\UserRequest;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('pages.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $readonly = false;
        return view('pages.users.edit-add',compact('readonly','edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username'=>['required','unique:users,username'],
            'name'=>['required','string'],
            'password' =>'required|confirmed',
            "password_confirmation" => ["string", "required"],
        ]);
        $data = $request->except(['_token']);
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $readonly = true;
        $edit = false;
        return view('pages.users.edit-add',compact('user','readonly','edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $edit = true;
        $readonly = false;
        return view('pages.users.edit-add',compact('user','edit','readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'username'=>['required',Rule::unique('users')->ignore($id)],
            'name'=>['required','string'],
            'password' =>'confirmed',
            'password_confirmation' => 'required_with:password'
        ]);
        $data = $request->except(['_method','_token']);
        if ($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        }
        $user = User::findOrFail($id)->update($data);
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route('users.index');
    }
}
