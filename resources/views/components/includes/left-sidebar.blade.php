<div class="left-side-menu">

    <div class="slimscroll-menu">


        <div class="user-box text-center">
            <img src="{{asset('assets/images/users/defult.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-thumbnail avatar-lg">
            <div class="dropdown">
                <a href="#" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{user()->name}}</a>
                <div class="dropdown-menu user-pro-dropdown">

                </div>
            </div>

        </div>


        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <x-UI.lift-sidebar.link
                    title="{{__('Dashboard')}}"
                    icon="mdi mdi-view-dashboard"
                    :url="route('dashboard')"
                />

                <x-UI.lift-sidebar.link
                    title="{{__('Users')}}"
                    icon="fas fa-user-shield"
                    :url="route('users.index')"
                />

                <x-UI.lift-sidebar.link
                    title="{{__('Images')}}"
                    icon="mdi mdi-image"
                    :url="route('images.index')"
                />
            </ul>

        </div>


        <div class="clearfix"></div>

    </div>


</div>
