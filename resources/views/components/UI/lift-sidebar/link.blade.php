<li>
    <a href="{{$url}}">
        <i class="{{$icon}}"></i>
        
        <span> {{$title}} </span>
    </a>
    {{$slot}}
</li>
